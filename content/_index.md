---
title: "Home"
date: 2018-02-10T18:56:13-05:00
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
<p>A procurement professional currently <a href="https://www.codecademy.com/arcJumper93280" target="_blank">conquering</a> his underdogs and embracing web and data technologies.</p>
